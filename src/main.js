// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
// import { securedAxiosInstance } from './backend/axios'
import './main.css'
import 'bootstrap/dist/css/bootstrap.css'
// import * as bootstrap from 'bootstrap'
// const bootstrap = require('bootstrap')
// import * as bootstrap from "bootstrap/dist/js/bootstrap.js"

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
const baseURL = 'http://localhost:3000'
if (typeof baseURL !== 'undefined') {
  Vue.axios.defaults.baseURL = baseURL
}
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
