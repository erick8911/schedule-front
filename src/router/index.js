import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Services from '@/components/services/Services.vue'
import Service from '@/components/services/Service.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }, {
      path: '/services',
      name: 'Services',
      component: Services
    }, {
      path: '/service/:id',
      name: 'Service',
      component: Service
    }
  ]
})
